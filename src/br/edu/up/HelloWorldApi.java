package br.edu.up;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello2")
public class HelloWorldApi {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public HelloWorldObject getHelloWorld() {
		return new HelloWorldObject();
	}
}
